
def handler(context, args):
    if 'nombre' in args.keys():
        a = []
        for character in args['nombre']:
            a.append(character)
        return { 'result': 'OK', 'message': a }
    else:
        return { 'result': 'Error', 'message' : 'no hay atributo nombre' }
